process altanalyze {

  publishDir "${params.samps_out_dir}/multisample/taltanalyze"
  label 'altanalyze_container'
  label 'altanalyze'

  input:
  path bams
  val parstr

  output:
  path("counts.original.pruned.txt"), emit: counts

  script:
  """
  mkdir -p bams
  mv *bam bams
  cp -r /usr/src/app/altanalyze .
  chmod -R 770 altanalyze
  chmod -R +x altanalyze
  cp -r /usr/src/app/AltAnalyze.sh .
  ./AltAnalyze.sh identify bams ${task.cpus}
  cp altanalyze_output/ExpressionInput/counts.original.pruned.txt .
  """
}

process combine_altanalyze_counts_with_alleles {

  label 'altanalyze_container'
  label 'altanalyze'

  input:
  path counts
  path alleles

  output:
  path("snaf.hla_alleles"), emit: snaf_alleles

  script:
  """
  echo "sample	hla" > snaf.hla_alleles
  for i in `head -1 counts.original.pruned.txt | sed -z 's/\\t/\\n/g'`; do
    SAMP=`echo \${i}`; ALLELES=`cat \${i%\\.Aligned*}*alleles | sed 's/HLA-A/HLA-A\\*'/g | sed 's/HLA-B/HLA-B\\*/g' | sed 's/HLA-C/HLA-C\\*/g'`; echo "\${SAMP}	\${ALLELES}" >> snaf.hla_alleles;
  done
  """
}

process snaf {

  publishDir "${params.samps_out_dir}/multisample/snaf"
  label "snaf_container"
  label "snaf"

  input:
  path counts
  path hla_alleles
  path refs
  path netmhcpan_dir

  output:
  path(result), emit: results

  script:
  """
#!/usr/bin/env python3

import os,sys
import pandas as pd
import numpy as np
import anndata as ad
import snaf
import subprocess

os.environ['NETMHCpan'] = os.path.realpath(os.path.join(os.getcwd(), 'netMHCpan-4.1/Linux_x86_64'))
os.environ['TMPDIR'] = "."

# read in the splicing junction matrix
df = pd.read_csv("${counts}",index_col=0,sep='\t')

# database directory (where you extract the reference tarball file) and netMHCpan folder
db_dir = "${refs}"
netMHCpan_path = "${netmhcpan_dir}/Linux_x86_64/bin/netMHCpan"
subprocess.run("{}".format(netMHCpan_path), shell=True)

# demonstrate how to add additional control database, see below note for more
tcga_ctrl_db = ad.read_h5ad(os.path.join("${refs}",'controls','tcga_matched_control_junction_count.h5ad'))
#gtex_skin_ctrl_db = ad.read_h5ad(os.path.join("${refs}",'controls','gtex_skin_count.h5ad'))
add_control = {'tcga_control':tcga_ctrl_db}

# initiate
snaf.initialize(df=df,db_dir=db_dir,binding_method='netMHCpan',software_path=netMHCpan_path,add_control=add_control)
#snaf.initialize(df=df,db_dir=db_dir,binding_method='MHCflurry',software_path='',add_control=add_control)

jcmq = snaf.JunctionCountMatrixQuery(junction_count_matrix=df,cores=${task.cpus},add_control=add_control,outdir='result')

sample_to_hla = pd.read_csv("${hla_alleles}",sep='\t',index_col=0)['hla'].to_dict()
hlas = [hla_string.split(',') for hla_string in df.columns.map(sample_to_hla)]

jcmq.run(hlas=hlas,outdir='./result')

jcmq.generate_results(path='result/after_prediction.p',outdir='result')

#jcmq = snaf.JunctionCountMatrixQuery.deserialize('result/after_prediction.p')
#os.mkdir('./fasta')
#snaf.chop_normal_pep_db(fasta_path='../SNAF_ecosystem/snaf_aux/human_uniprot_proteome.fasta',output_path='./fasta/human_proteome_uniprot_9_10_mers_unique.fasta',mers=[9,10],allow_duplicates=False)
#for sample in df.columns:
#    jcmq.show_neoantigen_as_fasta(outdir='./fasta',name='neoantigen_{}.fasta'.format(sample),stage=3,verbosity=1,contain_uid=True,sample=sample)
#    snaf.remove_redundant('./fasta/neoantigen_{}.fasta'.format(sample),'./fasta/neoantigen_{}_unique.fasta'.format(sample))
#    snaf.compare_two_fasta(fa1_path='./fasta/human_proteome_uniprot_9_10_mers_unique.fasta',
#                        fa2_path='./fasta/neoantigen_{}_unique.fasta'.format(sample),outdir='./fasta',
#                        write_unique2=True,prefix='{}_'.format(sample))
"""
}

process split_snaf_by_sample {

  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/snaf"
  label 'snaf_container'
  tag "${dataset}/${pat_name}/${run}"

  input:
  tuple val(pat_name), val(run), val(dataset)
  path results
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}.splice.pep.fa"), emit: splice_fasta
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}.snaf_peps.tsv"), emit: snaf_peps

  script:
  """
  grep "${dataset}-${pat_name}-${run}." result/frequency_stage3_verbosity1_uid_gene_symbol_coord_mean_mle.txt > ${dataset}-${pat_name}-${run}.snaf_peps.tsv

  rm -f ${dataset}-${pat_name}-${run}.splice.pep.fa

  while read line; do
    CHECKSUM=`echo \${line} | md5sum | cut -f 1 -d ' '`;
    PEPTIDE=`echo \${line} | cut -f 1 -d ' ' | cut -f 1 -d ','`;
    SPLICE_DESCRIPT=`echo\${line} | cut -f 1 -d ' ' | cut -f 2 -d ','`;
    NUM_SAMPLE=`echo \${line} | cut -f 3 -d ' '`;
    GENE_SYMBOL=`echo \${line} | cut -f 5 -d ' '`;
    SPLICE_COORDS=`echo \${line} | cut -f 6 -d ' '`;
    SNAF_TUMOR_SPEC_MEAN=`echo \${line} | cut -f 7 -d ' ' | cut -c -5`;
    SNAF_TUMOR_SPEC_MLE=`echo \${line} | cut -f 8 -d ' ' | cut -c -5`;
    echo ">\${CHECKSUM}" >> ${dataset}-${pat_name}-${run}.splice.pep.fa
    echo ";splice_description:\${SPLICE_DESCRIPT} gene_symbol:\${GENE_SYMBOL} splice_coords:\${SPLICE_COORDS} snaf_tumor_specificity_mean:\${SNAF_TUMOR_SPEC_MEAN} snaf_tumor_specificity_mle:\${SNAF_TUMOR_SPEC_MLE}" >> ${dataset}-${pat_name}-${run}.splice.pep.fa
    echo "\${PEPTIDE}" >> ${dataset}-${pat_name}-${run}.splice.pep.fa
  done < ${dataset}-${pat_name}-${run}.snaf_peps.tsv
  """
}
